## Student
# * `Student#initialize` should take a first and last name.
# * `Student#name` should return the concatenation of the student's
#   first and last name.
# * `Student#courses` should return a list of the `Course`s in which
#   the student is enrolled.
# * `Student#enroll` should take a `Course` object, add it to the
#   student's list of courses, and update the `Course`'s list of
#   enrolled students.
#     * `enroll` should ignore attempts to re-enroll a student.
# * `Student#course_load` should return a hash of departments to # of
#   credits the student is taking in that department.

class Student

  attr_reader :first_name, :last_name, :courses

  def initialize(first, last)
    @first_name = first
    @last_name = last
    @courses = []
  end

  def name
    @first_name + " " + @last_name
  end

  def enroll(new_course)
    if !@courses.include?(new_course)
      @courses << new_course
      new_course.students << self
    end
  end

  def course_load
    credits = Hash.new(0)
    @courses.each do |c|
      credits[c.department] += c.credits
    end
    credits
  end

end
